
import React from 'react';


import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import WebViewJs from './Screens/WebView'
const Stack = createStackNavigator();

const App = () =>{
  return (
   
      <NavigationContainer>
        <Stack.Navigator
          initialRouteName="WebView">

        <Stack.Screen 
          name="WebView" 
          component={WebViewJs} 
          options={{title: '',headerTitleAlign: 'center',headerTitleStyle: {fontWeight: 'normal'}, headerShown: false}}></Stack.Screen>

        </Stack.Navigator>
      </NavigationContainer>
    
  );
}



export default App;