import React, { useEffect, useState } from 'react'
import { StyleSheet,Image, View,PermissionsAndroid,BackHandler,Alert,ScreenOrientation} from 'react-native'
import WebView from 'react-native-webview'
import Orientation from 'react-native-orientation-locker'

const WebViewJs =()=>{
    const [cameraPermission, setCameraPermission] = useState(false); //check camera permission

   Orientation.lockToPortrait();
   async function requestPermissionsFromUser(){
      
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        'title': 'GudExams App',
        'message': 'App needs access to your camera.',
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera")
      setCameraPermission(true)
    } else {
      console.log("camera permission denied")
      setCameraPermission(false)
    }
  
}
    useEffect(()=>{
        (async () => { 
        permission = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: "Camera Permission",
            message:
              "App needs access to your camera.",
            buttonNegative: "Cancel",
            buttonPositive: "OK"
          }
        );
        })
      },[])

      useEffect(() => {        
        (async () => {        
         
          PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.CAMERA).then(response =>{
            if(response){
              console.log("Per reposnse "+response);
              setCameraPermission(true); 
            }else{
              console.log("Per reposnse "+response);
              setCameraPermission(false);
              requestPermissionsFromUser()
            }
          })
        })();        
    }, []);
    useEffect(() => {
      const backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want to exit the app?", [
          {
            text: "Cancel",
            onPress: () => null,
            style: "cancel"
          },
          { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
        return true;
      };
  
      const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
      );
  
      return () => backHandler.remove();
    }, []);
  
    return(
        <View style = {styles.container}> 
            
            <WebView 
                source={{uri: 'https://gpp.gudexams.com/'}} 
                javaScriptEnabled={true}
                domStorageEnabled={true}
                mediaPlaybackRequiresUserAction={false}  />
        </View>
        
    )
}
const styles = StyleSheet.create({

    container: {
      flex: 1,
      backgroundColor: 'white',      
     
    }
})
export default WebViewJs;